
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content">
					<section id="page-title">
						<div class="container">
							<div class="row">
								<div class="col-sm-8">
									<h1 class="mainTitle">Left Sidebar Blog Page</h1>
									<span class="mainDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing.</span>
								</div>
							</div>
						</div>
					</section>
					<section class="container-fluid container-fullw bg-white">
						<div class="container">
							<div class="row">

								<section class="breadcrumb-page">
						            <ol class="breadcrumb">
						              <li><a href="#">Home</a></li>
						              <li><a href="#">Landing Page</a></li>
						              <li class="active">Detail Page</li>
						            </ol>
							    </section>

								
								<div class="col-md-12">
									<h2 class="text-left">Our Peolple</h2>
									<h4>Meet the team</h4>

									<div class="row">
										<div data-appears-group-delay="0" data-appears-delay-increase="200">
											<ul class="team-list list-unstyled">
												<li class="col-sm-4">
													<div class="opacity-0 no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
														<div class="thumb icons-effect margin-bottom-15">
															<img alt="" src="assets/images/our-peolple/team-1.jpg" class="img-responsive">
															<div class="mask">
																<div class="icons-wrapper">
																	<div class="icons">
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-twitter fa-stack-1x fa-inverse"></i> </span> </a>
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-facebook fa-stack-1x fa-inverse"></i> </span> </a>
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i> </span> </a>
																	</div>
																</div>
															</div>
														</div>
														<h3>Peter Clark</h3>
														<h5>Managing Director</h5>
														
													</div>
												</li>
												<li class="col-sm-4">
													<div class="opacity-0 no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="200">
														<div class="thumb icons-effect margin-bottom-15">
															<img alt="" src="assets/images/our-peolple/team-2.jpg" class="img-responsive">
															<div class="mask">
																<div class="icons-wrapper">
																	<div class="icons">
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-twitter fa-stack-1x fa-inverse"></i> </span> </a>
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-github fa-stack-1x fa-inverse"></i> </span> </a>
																	</div>
																</div>
															</div>
														</div>
														<h3>Bob Frazier</h3>
														<h5>Manangin Director</h5>
														
													</div>
												</li>
												<li class="col-sm-4">
													<div class="opacity-0 no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="400">
														<div class="thumb icons-effect margin-bottom-15">
															<img alt="" src="assets/images/our-peolple/team-3.jpg" class="img-responsive">
															<div class="mask">
																<div class="icons-wrapper">
																	<div class="icons">
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-twitter fa-stack-1x fa-inverse"></i> </span> </a>
																	</div>
																</div>
															</div>
														</div>
														<h3>Nicole Bell</h3>
														<h5>Manangin Director</h5>
														
													</div>
												</li>
											</ul>
										</div>
										<div data-appears-group-delay="0" data-appears-delay-increase="200">
											<ul class="team-list list-unstyled">
												<li class="col-sm-4">
													<div class="opacity-0 no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
														<div class="thumb icons-effect margin-bottom-15">
															<img alt="" src="assets/images/our-peolple/team-1.jpg" class="img-responsive">
															<div class="mask">
																<div class="icons-wrapper">
																	<div class="icons">
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-twitter fa-stack-1x fa-inverse"></i> </span> </a>
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-facebook fa-stack-1x fa-inverse"></i> </span> </a>
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i> </span> </a>
																	</div>
																</div>
															</div>
														</div>
														<h3>Peter Clark</h3>
														<h5>Manangin Director</h5>
														
													</div>
												</li>
												<li class="col-sm-4">
													<div class="opacity-0 no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="200">
														<div class="thumb icons-effect margin-bottom-15">
															<img alt="" src="assets/images/our-peolple/team-2.jpg" class="img-responsive">
															<div class="mask">
																<div class="icons-wrapper">
																	<div class="icons">
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-twitter fa-stack-1x fa-inverse"></i> </span> </a>
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-github fa-stack-1x fa-inverse"></i> </span> </a>
																	</div>
																</div>
															</div>
														</div>
														<h3>Bob Frazier</h3>
														<h5>Manangin Director</h5>
														
													</div>
												</li>
												<li class="col-sm-4">
													<div class="opacity-0 no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="400">
														<div class="thumb icons-effect margin-bottom-15">
															<img alt="" src="assets/images/our-peolple/team-3.jpg" class="img-responsive">
															<div class="mask">
																<div class="icons-wrapper">
																	<div class="icons">
																		<a class="icon" href="#"> <span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-twitter fa-stack-1x fa-inverse"></i> </span> </a>
																	</div>
																</div>
															</div>
														</div>
														<h3>Nicole Bell</h3>
														<h5>Manangin Director</h5>
														
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
																
							
								<hr/>

								<article>
									<div class="row">
										<div class="col-md-10">
											<h2 class="text-left">Lid est laborum dolo rumes fugats untras</h2>
											<p>Etharums ser quidem rerum facilis dolores</p>
										</div>
										<div class="col-md-2">
											<button type="button" class="btn btn-dark btn-dark-grey margin-top-20">Call to Action</button>
										</div>
									</div>
								</article>

								<hr/>

							</div>
						</div>
					</section>
				</div>
				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
