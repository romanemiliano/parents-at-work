
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content temp-5">

					<section id="page-title">
						<div class="container">
							<div class="row">
								<div class="col-sm-6 padding-top-30">
									<h1 class="mainTitle">Access the latest resources for <br/> Working parents & employers</h1>
								</div>
							</div>
						</div>
					</section>


					<section class="container-fluid bg-white padding-top-20">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<div class="margin-bottom-30">
										<img src="assets/images/slider/slide1.jpg" class="img-responsive" alt="">
									</div>
								</div>
								<div class="col-sm-6">
									<h1 class="mainTitle">Title of most recent article here</h1>
									<p class="mainDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
									<p class="mainDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing.</p>
									<button type="button" class="btn btn-wide btn-dark margin-top-20">Read More</button>
					            </div>
				            </div>
						</div>
					</section>	


			        <section class="filters form-group">
		        		<label>Filter articles by:</label>
						 <select class="selectpicker" data-style="btn-primary">
						      <option>Option 1</option>
						      <option>Option 2</option>
						      <option>Option 3</option>
						 </select>
					</section> 
					

					<section class="container-fluid container-fullw bg-white">
						<div class="container">
							<div class="row">

								<div class="col-sm-12">

							        <!-- Filter Results -->
							        <ul id="result-events">
							        	<li>								
							        		<div class="col-md-4">
												<div class="thumbnail"> 
							                        <img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="" /> 
							                        <div class="caption">
							                            <h3><a href="#">Thumbnail label</a></h3> 
							                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
							                    		<p><button type="button" class="btn btn-wide btn-dark">Dark Grey</button></p>
							                        </div> 
							                    </div>
							                </div>
							            </li>
							        	<li>								
							        		<div class="col-md-4">
												<div class="thumbnail"> 
							                        <img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="" /> 
							                        <div class="caption">
							                            <h3><a href="#">Thumbnail label</a></h3> 
							                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
							                    		<p><button type="button" class="btn btn-wide btn-dark">Dark Grey</button></p>
							                        </div> 
							                    </div>
							                </div>
							            </li>
							        	<li>								
							        		<div class="col-md-4">
												<div class="thumbnail"> 
							                        <img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="" /> 
							                        <div class="caption">
							                            <h3><a href="#">Thumbnail label</a></h3> 
							                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
							                    		<p><button type="button" class="btn btn-wide btn-dark">Dark Grey</button></p>
							                        </div> 
							                    </div>
							                </div>
							            </li>
							        	<li>								
							        		<div class="col-md-4">
												<div class="thumbnail"> 
							                        <img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="" /> 
							                        <div class="caption">
							                            <h3><a href="#">Thumbnail label</a></h3> 
							                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
							                    		<p><button type="button" class="btn btn-wide btn-dark">Dark Grey</button></p>
							                        </div> 
							                    </div>
							                </div>
							            </li>
							        	<li>								
							        		<div class="col-md-4">
												<div class="thumbnail"> 
							                        <img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="" /> 
							                        <div class="caption">
							                            <h3><a href="#">Thumbnail label</a></h3> 
							                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
							                    		<p><button type="button" class="btn btn-wide btn-dark">Dark Grey</button></p>
							                        </div> 
							                    </div>
							                </div>
							            </li>
							        	<li>								
							        		<div class="col-md-4">
												<div class="thumbnail"> 
							                        <img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="" /> 
							                        <div class="caption">
							                            <h3><a href="#">Thumbnail label</a></h3> 
							                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
							                    		<p><button type="button" class="btn btn-wide btn-dark">Dark Grey</button></p>
							                        </div> 
							                    </div>
							                </div>
							            </li>										        
							        </ul>	
							        <!-- ./ End Filter Results -->

							        <div class="col-sm-12">
							        	<button type="button" class="btn btn-wide btn-dark pull-right">Older ></button>
							        </div>	
							    </div>

								<!-- Content -->
								<div class="col-md-12">
									<div class="blog-posts">

										<article>
											<div class="row">
												<div class="col-md-12">
													<h2 class="text-left">Page Title</h2>
												</div>
												<div class="col-md-12">
													<h4 class="over-title margin-bottom-15">Sub Title (Optional)</h4>
												</div>	
												<div class="col-md-6">
													<p>
														Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.						Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
													</p>
												</div>
												<div class="col-md-6">
													<p>
														Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.

													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
													</p>
												</div>
											</div>
										</article>

										<hr/>

										<article>
											<div class="row">
												<div class="col-md-10">
													<h2 class="text-left">Lid est laborum dolo rumes fugats untras</h2>
													<p>Etharums ser quidem rerum facilis dolores</p>
												</div>
												<div class="col-md-2">
													<button type="button" class="btn btn-wide btn-dark margin-top-20">Call to Action</button>
												</div>
											</div>
										</article>

										<hr/>

									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
