
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content">
					
					<!-- Slide top -->
					<section id="page-title" class="home"> 
						<div class="container">
							<div class="row">
								<div class="col-sm-7 pull-right">
									<h1 class="mainTitle animated fadeInDown delay">Creating family friendly workplaces of the future.</h1>
									<span class="animated fadeInUp delay">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing.</span>
									<div class="padding-top-25">
										<div class="clearfix">
				                        	<a class="btn btn-wide btn-green animated fadeIn delay" href="#">Trial our programs for free</a> 
				                        </div>
										<div class="clearfix">
											<a class="btn play-video animated fadeIn delay" data-toggle="modal" data-target="#videoModal">
											<i class="fa fa-play-circle" aria-hidden="true"></i>
											<span>LEARN MORE</span>
										</a>
				                    </div>
								</div>
							</div>
						</div>
					</section>
					<!-- /. Slide top -->


					<!-- Features & Benefits -->
					<section class="container-fluid container-fullw bg-white padding-bottom-30 benefits-box">
						<div class="container">
							
							<div class="margin-top-30 margin-bottom-30">	
								<h2 class="center no-visible" data-appears-class="fadeInUp" data-appears-delay="300">Membership Features & Benefits</h2>
								<hr>
								<div class="no-visible" data-appears-class="fadeIn" data-appears-delay="300">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>
								</div>
							</div>
						

							<div class="row">
								<div class="content-bxs" data-appears-group-delay="0" data-appears-delay-increase="100">
									<div class="col-sm-4 bx-mfb">
										<div class="panel green-bg1 no-radius text-center no-visible" data-appears-class="fadeIn">
											<div class="panel-body">
												<span><img src="assets/images/icons/icon_1.jpg"></span>
												<h4 class="StepTitle"><a href="#">Online <br/> Leanding Centre</a></h4>
											</div>
										</div>
									</div>
									<div class="col-sm-4 bx-mfb">
										<div class="panel green-bg2 no-radius text-center no-visible" data-appears-class="fadeIn">
											<div class="panel-body">
												<span><img src="assets/images/icons/icon_2.jpg"></span>
												<h4 class="StepTitle"><a href="#">Career <br/> Coaching</a></h4>
											</div>
										</div>
									</div>
									<div class="col-sm-4 bx-mfb">
										<div class="panel grey-bg1 no-radius text-center no-visible" data-appears-class="fadeIn">
											<div class="panel-body">
												<span><img src="assets/images/icons/icon_3.jpg"></span>
												<h4 class="StepTitle"><a href="#">Parent <br/> Concierge</a></h4>
											</div>
										</div>
									</div>

									<div class="col-sm-4 bx-mfb">
										<div class="panel grey-bg2 no-radius text-center no-visible" data-appears-class="fadeIn">
											<div class="panel-body">
												<span><img src="assets/images/icons/icon_4.jpg"></span>
												<h4 class="StepTitle"><a href="#">Special <br/>Events</a></h4>
											</div>
										</div>
									</div>
									<div class="col-sm-4 bx-mfb">
										<div class="panel green-bg3 no-radius text-center no-visible" data-appears-class="fadeIn">
											<div class="panel-body">
												<span><img src="assets/images/icons/icon_5.jpg"></span>
												<h4 class="StepTitle"><a href="#">Monthly <br/> Webinars</a></h4>
											</div>
										</div>
									</div>
									<div class="col-sm-4 bx-mfb">
										<div class="panel green-bg4 no-radius text-center no-visible" data-appears-class="fadeIn">
											<div class="panel-body">
												<span><img src="assets/images/icons/icon_6.jpg"></span>
												<h4 class="StepTitle"><a href="#">Manager <br/> Training</a></h4>
											</div>
										</div>
									</div>

								</div>
							</div>

							<div class="col-sm-12 text-center margin-top-30">
								<button type="button" class="btn btn-wide btn-violet">View our Membership Packages</button>
							</div>

						</div>
					</section>
					<!-- /. Features & Benefits -->


					<!-- /. Parents Program -->
					<section id="program-content" class="background-cnt" style="background-image: url('assets/images/slider/slide2.jpg')">
						<div class="container">
							<div class="row">
								<div class="col-sm-7">
									<h1 class="mainTitle text-white">
										Parents At Work is Australia's<br/> 
										leading provider of working<br/>
										parent programs
									</h1>
									<span class="mainDescription">
										Lorem ipsum dolor sit amet, feugiat delicata liberavisse id cum, no quo maiorum intellegebat, liber regione eu sit. Mea cu case ludus integre, vide viderer eleifend ex mea. His ay diceret, cum et atqui placerat.
									</span>
									<a href="#" class="btn btn-wide btn-violet">Learn More</a>
								</div>

							</div>
						</div>
					</section>
					<!-- /. Parents Program -->

					<!-- Special Events -->
					<section class="container-fluid container-fullw bg-white events-home">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h2 class="text-center padding-bottom-20" data-appears-class="fadeInDown" data-appears-delay="300">Upcomming Special Events</h2>
									<span data-appears-delay="300" data-appears-class="fadeIn" class="text-btm padding-bottom-30">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus. 
									</span>
								</div>
								<div class="col-sm-12 list-events" data-appears-group-delay="0" data-appears-delay-increase="300">
									<div class="col-md-4 no-visible" data-appears-class="fadeIn">
										<div class="thumbnail"> 
					                        <img src="assets/images/img_1.jpg" class="img-responsive" alt="" /> 
					                        <div class="caption">
					                            <h3><a href="#">Navigation you Career</a></h3> 
					                            <p>23 February | <b>2-3PM AEST</b></p>  
					                    		<a href="#" class="btn btn-wide btn-violet">Register Now</a>
					                        </div> 
					                    </div>
									</div>
									<div class="col-md-4 no-visible" data-appears-class="fadeIn">
										<div class="thumbnail"> 
					                        <img src="assets/images/img_2.jpg" class="img-responsive" alt="" /> 
					                        <div class="caption">
					                            <h3><a href="#">Navigation you Career</a></h3> 
					                            <p>23 February | <b>2-3PM AEST</b></p> 
					                    		<a href="#" class="btn btn-wide btn-violet">Register Now</a>
					                        </div> 
					                    </div>
									</div>
									<div class="col-md-4 no-visible" data-appears-class="fadeIn">
										<div class="thumbnail"> 
					                        <img src="assets/images/img_3.jpg" class="img-responsive" alt="" /> 
					                        <div class="caption">
					                            <h3><a href="#">Navigation you Career</a></h3> 
					                            <p>23 February | <b>2-3PM AEST</b></p> 
					                    		<span class="text-center"><a href="#" class="btn btn-wide btn-violet">Register Now</a></span>
					                        </div> 
					                    </div>									
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- /. Special Events -->

					<!-- Testimonials -->
					<section class="container-fluid container-fullw bg-white testimonial">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
						            <div class="owl-carousel owl-theme">
						                <div class="item">
											<h4 class="no-visible" data-appears-class="fadeIn" data-appears-delay="100">"Lorem ipsum dolor sit amet, feugiat delicata liberavisse id cum, no quo maiorum intellegebat, liber regione eu sit. Mea cu case ludus integre, vide viderer eleifend ex mea. His ay diceret, cum et atqui placerat. Lorem ipsum dolor sit amet, feugiat delicata liberavisse id cum, no quo maiorum intellegebat, liber regione eu sit. Mea cu case ludus integre, vide viderer eleifend ex mea. His ay diceret, cum et atqui placerat."</h4>
											<p class="testimonial_author">
												<i>Kristie Holmes</i> | Human resurces Manager | The westpac Group
											</p>
											<p><span class="text-center"><a href="#" class="btn btn-wide btn-violet">Read our case studies</a></span></p>
						                </div>

						                <div class="item">
											<h4>"Lorem ipsum dolor sit amet, feugiat delicata liberavisse id cum, no quo maiorum intellegebat, liber regione eu sit. Mea cu case ludus integre, vide viderer eleifend ex mea. His ay diceret, cum et atqui placerat. Lorem ipsum dolor sit amet, feugiat delicata liberavisse id cum, no quo maiorum intellegebat, liber regione eu sit. Mea cu case ludus integre, vide viderer eleifend ex mea. His ay diceret, cum et atqui placerat."</h4>
											<p class="testimonial_author">
												<i>Kristie Holmes</i> | Human resurces Manager | The westpac Group
											</p>
											<p><span class="text-center"><a href="#" class="btn btn-wide btn-violet">Read our case studies</a></span></p>
						                </div>

						                <div class="item">
											<h4>"Lorem ipsum dolor sit amet, feugiat delicata liberavisse id cum, no quo maiorum intellegebat, liber regione eu sit. Mea cu case ludus integre, vide viderer eleifend ex mea. His ay diceret, cum et atqui placerat. Lorem ipsum dolor sit amet, feugiat delicata liberavisse id cum, no quo maiorum intellegebat, liber regione eu sit. Mea cu case ludus integre, vide viderer eleifend ex mea. His ay diceret, cum et atqui placerat."</h4>
											<p class="testimonial_author">
												<i>Kristie Holmes</i> | Human resurces Manager | The westpac Group
											</p>
											<p><span class="text-center"><a href="#" class="btn btn-wide btn-violet">Read our case studies</a></span></p>
						                </div>
						            </div>
								</div>
							</div>
						</div>
					</section>
					<!-- /. Testimonials -->

				</div>


				<!-- Modal Video -->
				<div class="modal fade bs-example-modal-lg" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
				    <div class="modal-dialog modal-lg">
				        <div class="modal-content">
				            <div class="modal-body">
				                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				                <div>
				                    <iframe width="100%" height="350" src="" allowfullscreen></iframe>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>

				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
