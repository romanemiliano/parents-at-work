
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content">
					<section id="page-title">
						<div class="container">
							<div class="row">
								<div class="col-sm-8 padding-top-30">
									<h1 class="mainTitle">Up to two linecaption<br/>describing Content Template</h1>
									<p class="margin-top-30"><a href="#" class="btn btn-wide btn-dark">Call to Action</a></p>
								</div>
							</div>
						</div>
					</section>
					<section class="container-fluid container-fullw bg-white">
						<div class="container">
							<div class="row">

								<section class="breadcrumb-page">
						            <ol class="breadcrumb">
						              <li><a href="#">Home</a></li>
						              <li><a href="#">Landing Page</a></li>
						              <li class="active">Detail Page</li>
						            </ol>
							    </section>

								<!-- Content -->
								<div class="col-md-12">
									<div class="temp-4">

										<article>
											<div class="row">
												<div class="col-md-12">
													<h2 class="text-left">Page Title</h2>
												</div>
												<div class="col-md-12">
													<h4 class="over-title margin-bottom-15">Sub Title (Optional)</h4>
												</div>	
												<div class="col-md-6">
													<p>
														Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.						Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
													</p>
												</div>
												<div class="col-md-6">
													<p>
														Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.

													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
													</p>
												</div>
											</div>
										</article>

										<hr/>



										<article>
											<div class="row">
												<div class="col-md-12 related-items">
													<h2 class="text-left">Another Sub Title</h2>
													<p>You may also interesed in thes course</p>
													
													<div data-appears-group-delay="0" data-appears-delay-increase="300">
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Two Lines</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Two Lines</a></p>
																</div>
															</div>
														</div>
													</div>

													<div data-appears-group-delay="0" data-appears-delay-increase="300">
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Two Lines</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Two Lines</a></p>
																</div>
															</div>
														</div>
													</div>

												</div>
											</div>
										</article>									

										<hr />


										<article>
											<div class="row">
												<div class="col-md-10">
													<h2 class="text-left">Lid est laborum dolo rumes fugats untras</h2>
													<p>Etharums ser quidem rerum facilis dolores</p>
												</div>
												<div class="col-md-2">
													<button type="button" class="btn btn-wide btn-dark margin-top-20">Call to Action</button>
												</div>
											</div>
										</article>

										<hr/>

									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
