
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content">

					<section id="page-title">
						<div class="container">
							<div class="row">
								<div class="col-sm-12">
						            <ol class="breadcrumb">
						              <li><a href="#">Home</a></li>
						              <li><a href="#">Resourses</a></li>
						              <li class="active">New & Articles</li>
						              <li class="active">Article Title</li>
						            </ol>
							    </div>
								<div class="col-sm-12">
									<p>Topic Name</p>
									<h1 class="mainTitle">Article Title Goes Here <br/>maybe needs second line</h1>
								</div>
							</div>
						</div>
					</section>

					<section class="container-fluid container-fullw bg-white">
						<div class="container">
							<div class="row">

								<!-- Content -->
								<div class="col-md-12">
									<div class="articles-list" style="padding: 0 150px;">

										<article>
											<div class="row">
												<div class="col-md-12">
													<div class="margin-bottom-30">
														<img src="assets/images/slider/slide1.jpg" class="img-responsive" alt="">
													</div>
												</div>
												<div class="col-md-12">
													<div class="post-content">
														<h2><a href="#"> Euismod atras vulputate iltricies etri elit </a></h2>
														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.  sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.  sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae. [...]
														</p>

														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.  sed ligula. Nam dolor ligula [...]
														</p>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<button type="button" class="btn btn-wide btn-dark margin-top-20">Read More</button>
												</div>
											</div>
										</article>

										<hr/>


										<article>
											<div class="row">
												<div class="col-md-12">
													<div class="margin-bottom-30">
														<img src="assets/images/slider/slide1.jpg" class="img-responsive" alt="">
													</div>
												</div>
												<div class="col-md-12">
													<div class="post-content">
														<h2><a href="#"> Euismod atras vulputate iltricies etri elit </a></h2>
														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.  sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.  sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae. [...]
														</p>

														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.  sed ligula. Nam dolor ligula [...]
														</p>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<button type="button" class="btn btn-wide btn-dark margin-top-20">Read More</button>
												</div>
											</div>
										</article>

										<hr/>



										<article>
											<div class="row">
												<div class="col-md-12 related-items small">
													<h2 class="text-left">Related Articles</h2>
													<p>You may also be like to read these related articles</p>
													
													<div data-appears-group-delay="0" data-appears-delay-increase="300">
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Tow Lines Title Box content</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Two Lines</a></p>
																</div>
															</div>
														</div>
													</div>

												</div>
											</div>
										</article>									

										<hr />

									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
