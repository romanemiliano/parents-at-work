
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content">

					<section id="page-title">
						<div class="container">
							<div class="row">
								<div class="col-sm-6">
									<h1 class="mainTitle">Attend one of our free events to support working parents</h1>
								</div>
							</div>
						</div>
					</section>

					<section class="container-fluid container-fullw bg-white">
						<div class="container">
							<div class="row">

								<!-- Content -->
								<div class="col-md-12">
									
									<article>
										<div class="row">
									        <!-- Events list -->
									        <ul class="list-events">
									        	<li>								
									        		<div class="col-md-4">
														<div class="thumbnail"> 
									                        <img src="http://placehold.it/350x250" class="img-responsive" alt="" /> 
									                        <div class="caption">
									                            <h3><a href="#">Navigation you Career in 2017 </a></h3> 
									                            <p>23 February</p>
									                            <p>2-3PM AEST</p> 
									                    		<a href="#" class="btn btn-wide btn-dark">Register Now</a>
									                        </div> 
									                    </div>
									                </div>
									            </li>
									        	<li>								
									        		<div class="col-md-4">
														<div class="thumbnail"> 
									                        <img src="http://placehold.it/350x250" class="img-responsive" alt="" /> 
									                        <div class="caption">
									                            <h3><a href="#">Navigation you Career in 2017 </a></h3> 
									                            <p>23 February</p>
									                            <p>2-3PM AEST</p> 
									                    		<a href="#" class="btn btn-wide btn-dark">Register Now</a>
									                        </div> 
									                    </div>
									                </div>
									            </li>
									        	<li>								
									        		<div class="col-md-4">
														<div class="thumbnail"> 
									                        <img src="http://placehold.it/350x250" class="img-responsive" alt="" /> 
									                        <div class="caption">
									                            <h3><a href="#">Navigation you Career in 2017 </a></h3> 
									                            <p>23 February</p>
									                            <p>2-3PM AEST</p> 
									                    		<a href="#" class="btn btn-wide btn-dark">Register Now</a>
									                        </div> 
									                    </div>
									                </div>
									            </li>
									        	<li>								
									        		<div class="col-md-4">
														<div class="thumbnail"> 
									                        <img src="http://placehold.it/350x250" class="img-responsive" alt="" /> 
									                        <div class="caption">
									                            <h3><a href="#">Navigation you Career in 2017 </a></h3> 
									                            <p>23 February</p>
									                            <p>2-3PM AEST</p> 
									                    		<a href="#" class="btn btn-wide btn-dark">Register Now</a>
									                        </div> 
									                    </div>
									                </div>
									            </li>
									        	<li>								
									        		<div class="col-md-4">
														<div class="thumbnail"> 
									                        <img src="http://placehold.it/350x250" class="img-responsive" alt="" /> 
									                        <div class="caption">
									                            <h3><a href="#">Navigation you Career in 2017 </a></h3> 
									                            <p>23 February</p>
									                            <p>2-3PM AEST</p> 
									                    		<a href="#" class="btn btn-wide btn-dark">Register Now</a>
									                        </div> 
									                    </div>
									                </div>
									            </li>
									        	<li>								
									        		<div class="col-md-4">
														<div class="thumbnail"> 
									                        <img src="http://placehold.it/350x250" class="img-responsive" alt="" /> 
									                        <div class="caption">
									                            <h3><a href="#">Navigation you Career in 2017 </a></h3> 
									                            <p>23 February</p>
									                            <p>2-3PM AEST</p> 
									                    		<a href="#" class="btn btn-wide btn-dark">Register Now</a>
									                        </div> 
									                    </div>
									                </div>
									            </li>						        
									        </ul>	
									        <!-- ./ End Filter Results -->
										</div>
									</article>

									<hr/>

									<article>
										<div class="row">
											<div class="col-md-10">
												<h2 class="text-left">Lid est laborum dolo rumes fugats untras</h2>
												<p>Etharums ser quidem rerum facilis dolores</p>
											</div>
											<div class="col-md-2">
												<button type="button" class="btn btn-wide btn-dark margin-top-20">Call to Action</button>
											</div>
										</div>
									</article>

									<hr/>
									
								</div>
							</div>
						</div>
					</section>
				</div>
				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
