
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content temp-2">
					<section id="page-title">
						<div class="container">
							<div class="row">
								<div class="col-sm-8 padding-top-30">
									<h1 class="mainTitle">Up to two linecaption<br/>describing Content Template</h1>
									<p class="margin-top-30"><a href="#" class="btn btn-wide btn-dark">Call to Action</a></p>
								</div>
							</div>
						</div>
					</section>

					<section class="container-fluid container-fullw bg-white">
						<div class="container">
							<div class="row">
								
								<!-- Col Left Nav -->
								<div class="col-md-3">
									<nav class="hidden-xs hidden-sm affix-top">
										<ul class="nav">
											<li>
												<a href="#section-1" class="active-anchor">Title Section 1</a>
											</li>
											<li >
												<a href="#section-2" class="active-anchor">Title Section 2</a>
											</li>
											<li>
												<a href="#section-3" class="active-anchor">Title Section 3</a>
											</li>
											<li>
												<a href="#section-4" class="active-anchor">Title Section 4</a>
											</li>
										</ul>

									</nav>
								</div>

								<!-- Col Right Content -->
								<div class="col-md-9">
									<div class="articles">
										<article id="articles-list">
											<div class="row">
												<div class="col-md-12">
													<div class="post-content">
														<h2>Section Title</h2>
														<h4>Sub Title (Optional)</h4>
														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.
														</p>

														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.
														</p>

					                    				<p class="margin-top-30"><button type="button" class="btn btn-wide btn-dark">Call to Action</button></p>
													</div>
												</div>
											</div>										
										</article>

										<hr />

										<article id="section-2">
											<div class="row">
												<div class="col-sm-12">
													<h2>Section Title</h2>
												</div>	
												<div class="col-sm-7">
													<h4>Sub Title (Optional)</h4>
													<p class="text-dark">
														Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.
													</p>
													<p>	
														Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.
														Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos.
													</p>
												</div>
												<div class="col-sm-5">
													<div class="margin-top-30">
														<img src="https://dummyimage.com/335x225/cccccc" class="img-responsive" alt="">
													</div>
												</div>

												<div class="col-sm-12">
													<p class="margin-top-10"><button type="button" class="btn btn-wide btn-dark">Call to Action</button></p>
												</div>	
											</div>
										</article>

										<hr/>

										<article id="section-3">
											<div class="row">
												<div class="col-sm-12">	
													<h2>Section Title</h2>
													<h4>Sub Title (Optional)</h4>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.
													</p>

													<!-- Icons Cols -->
													<div class="margin-top-30" data-appears-group-delay="0" data-appears-delay-increase="300">
														<div class="col-sm-4">
															<div class="text-center">
																<div class="panel-body">
																	<span class="fa-stack fa-2x"> <i class="fa fa-android" aria-hidden="true"></i> </span>
																	<h4 class="textIcon">Lorem ipsum dolor<br/> sit am</h4>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center">
																<div class="panel-body">
																	<span class="fa-stack fa-2x"> <i class="fa fa-volume-down" aria-hidden="true"></i> </span>
																	<h4 class="textIcon">Lorem ipsum dolor<br/> sit am</h4>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center">
																<div class="panel-body">
																	<span class="fa-stack fa-2x"> <i class="fa fa-android" aria-hidden="true"></i> </span>
																	<h4 class="textIcon">Lorem ipsum dolor<br/> sit am</h4>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</article>

										<hr />

										<article id="section-4">
											<div class="row">
												<div class="col-md-12">
													<div class="margin-bottom-30">
														<h2>Section Title</h2>
														<h4>Sub Title (Optional)</h4>
														<p>
															Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.
														</p>
													</div>		
												</div>	

												<div class="col-md-12">
													<div id="accordion" class="panel-group accordion-custom">
														<div class="panel panel-transparent">
															<div class="panel-heading">
																<h4 class="panel-title"><a href="#faq_1_1" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle collapsed"> <i class="icon-arrow"></i> Excepteur sint obcaecat cupiditat non proident? </a></h4>
															</div>
															<div class="panel-collapse collapse" id="faq_1_1">
																<div class="panel-body">
																	Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
																</div>
															</div>
														</div>
														<div class="panel panel-transparent">
															<div class="panel-heading">
																<h4 class="panel-title"><a href="#faq_1_2" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle collapsed"> <i class="icon-arrow"></i> Quis aute iure reprehenderit in voluptate velit esse cillum? </a></h4>
															</div>
															<div class="panel-collapse collapse" id="faq_1_2">
																<div class="panel-body">
																	Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
																</div>
															</div>
														</div>
														<div class="panel panel-transparent">
															<div class="panel-heading">
																<h4 class="panel-title"><a href="#faq_1_3" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle collapsed"> <i class="icon-arrow"></i> Nemo enim ipsam voluptatem, quia voluptas sit? </a></h4>
															</div>
															<div class="panel-collapse collapse" id="faq_1_3">
																<div class="panel-body">
																	Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
																</div>
															</div>
														</div>
														<div class="panel panel-transparent">
															<div class="panel-heading">
																<h4 class="panel-title"><a href="#faq_1_4" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle collapsed"> <i class="icon-arrow"></i> Excepteur sint obcaecat cupiditat non proident? </a></h4>
															</div>
															<div class="panel-collapse collapse" id="faq_1_4">
																<div class="panel-body">
																	Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
																</div>
															</div>
														</div>
													</div>
												</div>	
												
											</div>
										</article>

										<hr />

									</div>
								</div>

							</div>
						</div>
					</section>
				</div>

				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
