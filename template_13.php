
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content">

					<section id="page-title">
						<div class="container">
							<div class="row">
								<div class="col-sm-8">
									<h1 class="mainTitle">We're here to help you<br/> support your working parents </h1>
								</div>
							</div>
						</div>
					</section>

					<section class="container-fluid container-fullw bg-white contact">
						<div class="container">
							<div class="row">
								
								<!-- Col Left -->
								<div class="col-md-3">
									<aside class="sidebar">
										<h4>Contact us</h4>
										<ul class="nav nav-list blog-categories">
											<li>
												<a href="#"> Contact </a>
											</li>
											<li>
												<a href="#"> Join to Day </a>
											</li>
										</ul>
									</aside>
								</div>

								
								<!-- Col Right -->
								<div class="col-md-9">
									<div class="col-md-12">
										<div class="padding-bottom-20">
											<h2>Contact Us</h2>
											<span class="mainDescription">Where you can find us</span>
										</div>

										<h2>The Office</h2>
										<ul class="list-unstyled">
											<li>
												<i class="icon icon-map-marker"></i><strong>Address:</strong> 1234 Street Name, City Name, United States
											</li>
											<li>
												<i class="icon icon-phone"></i><strong>Phone:</strong> (123) 456-7890
											</li>
											<li>
												<i class="icon icon-envelope"></i><strong>Email:</strong>
												<a href="mailto:mail@example.com"> mail@example.com </a>
											</li>
										</ul>
										<hr class="right">
										<h2>Business Hours</h2>
										<ul class="list-unstyled">
											<li>
												<i class="icon icon-time"></i> Monday - Friday 9am to 5pm
											</li>
											<li>
												<i class="icon icon-time"></i> Saturday - 9am to 2pm
											</li>
											<li>
												<i class="icon icon-time"></i> Sunday - Closed
											</li>
										</ul>

									</div>

									<hr/>
									
									<div class="col-md-12">
										<div class="alert alert-success hidden" id="contactSuccess">
											<strong>Success!</strong> Your message has been sent to us.
										</div>
										<div class="alert alert-error hidden" id="contactError">
											<strong>Error!</strong> There was an error sending your message.
										</div>
										<h2>Join Today</h2>
										<h3>We can help you better support your working parents</h3>
										<hr>

										<form novalidate="novalidate" action="" id="contactForm" type="post">
											<div class="row">
												<div class="form-group">
													<div class="col-md-6">
														<label> First Name <span class="symbol required"></span> </label>
														<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name">
													</div>
													<div class="col-md-6">
														<label> Last Name <span class="symbol required"></span> </label>
														<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name">
													</div>
													<div class="col-md-6">
														<label> Email address <span class="symbol required"></span> </label>
														<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email">
													</div>
													<div class="col-md-6">
														<label> Phone number <span class="symbol required"></span> </label>
														<input type="phone" value="" data-msg-required="Please enter your phone." data-msg-email="Please enter a valid phone." maxlength="100" class="form-control" name="phone" id="phone">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="form-group">
													<div class="col-md-12">
														<label> Message <span class="symbol required"></span> </label>
														<textarea maxlength="5000" data-msg-required="Please enter your message." rows="10" class="form-control" name="message" id="message"></textarea>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 margin-top-30 margin-bottom-30"><button type="button" class="btn btn-wide btn-dark">Submit</button></div>
											</div>
										</form>
									</div>
								</div>

							</div>
						</div>
					</section>
				</div>
				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
