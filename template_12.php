
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content">

					<section id="page-title">
						<div class="container">
							<div class="row">
								<div class="col-sm-12">
						            <ol class="breadcrumb">
						              <li><a href="#">Home</a></li>
						              <li><a href="#">Events</a></li>
						              <li><a href="#">Social Events</a></li>
						              <li>Events Title</li>
						            </ol>
							    </div>
								<div class="col-sm-12">
									<p>Thursday 15 February, 1pm-2am</p>
									<h1 class="mainTitle">Starting school in 2017 <br/> maybe needs second line</h1>
								</div>
							</div>
						</div>
					</section>

					<section class="container-fluid container-fullw bg-white">
						<div class="container">
							<div class="row">

								<!-- Content -->
								<div class="col-md-12">
									<div class="blog-posts" style="padding: 0 150px;">

										<article>
											<div class="row">
												<div class="col-md-12">
													<div class="margin-bottom-30">
														<img src="assets/images/img1.jpg" class="img-responsive" alt="">
													</div>
												</div>
												<div class="col-md-12">
													<div class="post-content">
														<h2><a href="#"> Euismod atras vulputate iltricies etri elit </a></h2>
														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.  sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.  sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae. [...]
														</p>

														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.  sed ligula. Nam dolor ligula [...]
														</p>
														
														<h5 class="text-left padding-top-20"><b>This course help organisation to:</b></h5>
															<ul>
																<li>Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers</li>
																<li>Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers Etharums ser quidem rerum </li>
																<li>Etharums ser quidem rerum facilis dolores nemis omnis</li>
																<li>Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers</li> 	
															</ul>

														<button class="btn btn-wide btn-dark animated fadeIn delay">Listen Now</button>

													</div>
												</div>
											</div>
										</article>

										<hr/>


										<article>
											<div class="row">
												<div class="col-sm-5">
													<div class="post-media margin-bottom-30">
														<img src="assets/images/img1.jpg" class="img-responsive margin-bottom-15" alt="">
													</div>
												</div>
												<div class="col-sm-7">
													<div class="post-content">
														<h2><a href="#"> Euismod atras vulputate iltricies etri elit </a></h2>
														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae. [...]
														</p>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-sm-5">
													<div class="post-media margin-bottom-30">
														<img src="assets/images/img1.jpg" class="img-responsive margin-bottom-15" alt="">
													</div>
												</div>
												<div class="col-sm-7">
													<div class="post-content">
														<h2><a href="#"> Euismod atras vulputate iltricies etri elit </a></h2>
														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae. [...]
														</p>
													</div>
												</div>
											</div>
										</article>

										<hr/>



										<article>
											<div class="row">
												<div class="col-md-12 related-items">
													<h2 class="text-left">Related Articles</h2>
													<p>You may also be like to read these related articles</p>
													
													<div data-appears-group-delay="0" data-appears-delay-increase="300">
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Tow Lines Title Box content</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Tow Lines Title Box content</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Tow Lines Title Box content</a></p>
																</div>
															</div>
														</div>
													</div>

												</div>
											</div>
										</article>									

										<hr />

									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
