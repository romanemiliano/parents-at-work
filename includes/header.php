			<!-- HEADER -->
			<header>
				<div class="navbar navbar-default" role="navigation">

					<div class="nav-top-right">
						<div class="container">
							<ul class="nav top">
								<li>
									<a href="#" class="member"> Member Login </a>
								</li>
								<li>
									<a href="#" class="contact"> Contact Us </a>
								</li>
								<li>
									<a href="#" class="join"> Join Today </a>
								</li>
							</ul>
						</div>	
					</div>
					
					<!-- START NAVIGATION -->
					<div class="container">
						<div class="navbar-header">
							<!-- MENU TOGGLER -->
							<a class="pull-left menu-toggler hidden-md hidden-lg mobile-button" id="menu-toggler" data-toggle="collapse" href=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <i class="ti-align-justify"></i> </a>
							<!-- /. MENU TOGGLER -->
							<!-- LOGO -->
							<a href="index.html" class="navbar-brand"> 
								<img src="assets/images/logo.jpg" alt="Logo">
							</a>
							<!-- /. LOGO -->
						</div>
						<!-- NAVBAR -->
						<div class="navbar-collapse collapse">
							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown mega-menu">
									<a data-toggle="dropdown" href="#" class="dropdown-toggle">  Our Service  </a>
									<ul class="dropdown-menu">
										<li>
											<!-- MEGA MENU CONTENT -->
											<div class="mega-menu-content">
												<div class="col-md-4">
													<span class="mega-menu-sub-title">Our Services</span>
													<p class="margin-top-20 margin-bottom-20"> Parents at Work provides over 100,000 families and their managers with accesess to the ultimate work and family support package. </p>
													<button class="btn btn-wide btn-violet animated fadeIn delay">Learn More</button>
												</div>
												<div class="col-md-4">
													<ul class="sub-menu">
														<li>
															<ul>
																<li>
																	<a href="#"> Working Parent Memberships </a>
																</li>
																<li>
																	<a href="#"> Career Coaching </a>
																</li>
																<li>
																	<a href="#"> Online Learning </a>
																</li>
																<li>
																	<a href="#"> Monthly Webinars</a>
																</li>	
															</ul>
														</li>
													</ul>
												</div>
												<div class="col-md-4">
													<ul class="sub-menu">
														<li>
															<ul>
																<li>
																	<a href="#"> Special Events </a>
																</li>
																<li>
																	<a href="#"> Career Concierge </a>
																</li>
																<li>
																	<a href="#"> Memberships benefits</a>
																</li>
																<li>
																	<a href="#"> The Parents At Work Difference</a>
																</li>	
															</ul>
														</li>
													</ul>
												</div>
											</div>
											<!-- /. MEGA MENU CONTENT -->
										</li>
									</ul>
								</li>
								<li>
									<a href="#"> About Us </a>
								</li>
								<li>
									<a href="#"> Events </a>
								</li>
								<li>
									<a href="#"> Resources </a>
								</li>
							</ul>
							<!-- start: MENU TOGGLER MOBILE -->
							<div class="close-handle visible-sm-block visible-xs-block menu-toggler" data-toggle="collapse" href=".navbar-collapse">
								<div class="arrow-left"></div>
								<div class="arrow-right"></div>
							</div>
							<!-- end: MENU TOGGLER MOBILE -->
						</div>
						<!-- /. NAVBAR -->
					</div>
					<!-- /. END NAVIGATION -->
				</div>
			</header>
			<!-- /. HEADER -->