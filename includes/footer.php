				

				<a id="scroll-top" href="#"><i class="fa fa-angle-up"></i></a>
				<!-- end: FOOTER -->

				
				<!-- FOOTER -->
				<footer id="footer">
					<div class="container">
						<div class="row">
							<div class="col-md-12 suscribe">
								<h4>Suscribe to our Newsletter:</h4>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.
								</p>

								<div class="col-lg-4 col-md-5 icons-ft">
									<div class="block-icons">
										<div class="icon"><img src="https://dummyimage.com/50x50/ffffff/ffffff" width="50"></div>	
										<span class="txt">I AM A PARENTS</span> 
									</div>
									<div class="block-icons">
										<div class="icon"><img src="https://dummyimage.com/50x50/ffffff/ffffff" width="50"></div>	
										<span class="txt">I AM A HR MANAGER</span>
									</div>
								</div>

								<div class="col-lg-8 col-md-7">
									<div class="form-group">
										<input type="text" placeholder="Email Address">
			                            <button class="btn btn-wide btn-violet">Suscribe for updates</button>
		                            </div>
	                            </div>
							</div>
						</div>
					</div>
					<div class="footer-nav">
						<div class="container">
							<div class="row">
								<div class="col-lg-9 col-md-9 col-xs-12">
									<ul class="nav-list">
										<li><a href="#">News & Articles</a></li>
										<li><a href="#">Special Events</a></li>
										<li><a href="#">Podcast</a></li>
										<li><a href="#">Media</a></li>
									</ul>
								</div>

								<div class="col-lg-3 col-md-3 col-xs-12">
									<span class="privacy"><a href="#">Privacy Policy</a></span>
								</div>
							</div>
						</div>
					</div>
					<div class="footer-copyright">
						<div class="container">
							<div class="row">
								<div class="col-md-8">
									<div class="social-icons">
										<ul>
											<li class="social-facebook">
												<a target="_blank" href="#"> Facebook </a>
											</li>
											<li class="social-twitter">
												<a target="_blank" href="#"> Twitter </a>
											</li>
											<li class="social-google">
												<a target="_blank" href="#"> Google + </a>
											</li>
											<li class="social-linkedin">
												<a target="_blank" href="#"> LinkedIn </a>
											</li>
										</ul>
									</div>
								</div>

								<div class="col-md-4 text-right">
									<span class="copy">
										&copy; Copyright Parents at Work <span class="current-year"></span> 
									</span>
								</div>
							</div>
						</div>
					</div>
				</footer>
				<!-- /. FOOTER -->	