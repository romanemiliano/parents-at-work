
		<!-- JavaScripts -->
		<script src="plugins/jquery/dist/jquery.min.js"></script>
		<script src="plugins/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
		<script src="plugins/sticky-kit/jquery.sticky-kit.min.js"></script>
		<script src="plugins/jquery.appear.js/jquery.appear.js"></script>
		<script src="plugins/slick.js/slick/slick.min.js"></script>
		<script src="plugins/jquery.stellar/jquery.stellar.min.js"></script>
		<script src="plugins/countto/jquery.countTo.js"></script>
		<script src="plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->

		<!-- Carousel -->
		<script src="assets/js/owl.carousel.js"></script>
		<script type="text/javascript">
		$('.owl-carousel').owlCarousel({
		    loop:true,
		    margin:10,
		    nav:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		})
		</script>

		<script src="assets/js/main.js"></script>
		<!-- /. JavaScripts -->

		<script src="assets/js/index.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
			});
		</script>
