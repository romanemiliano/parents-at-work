<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title> Parents At Work</title>
		<!-- start: META -->
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- /. meta-->

		<link rel="shortcut icon" href="favicon.ico" />

		<!-- CSS -->
		<link href="plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
		<link href="plugins/themify-icons/themify-icons.css" rel="stylesheet" media="screen">
		<link href="plugins/animate.css/animate.min.css" rel="stylesheet" media="screen">
		<link href="plugins/slick.js/slick/slick.css" rel="stylesheet" media="screen" />
		<link href="plugins/slick.js/slick/slick-theme.css" rel="stylesheet" media="screen" />
		<link href="plugins/swiper/dist/css/swiper.min.css" rel="stylesheet" media="screen" />

		<link href="assets/css/styles.css" rel="stylesheet" media="screen">
		<link href="assets/css/plugins.css" rel="stylesheet" media="screen">		
		<!-- Carousel -->
	    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
	</head>