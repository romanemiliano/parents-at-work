
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content">
					<section id="page-title">
						<div class="container">
							<div class="row">
								<div class="col-sm-8">
									<h1 class="mainTitle">Left Sidebar Blog Page</h1>
									<span class="mainDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing.</span>
								</div>
							</div>
						</div>
					</section>
					<section class="container-fluid container-fullw bg-white">
						<div class="container">

							<div class="row">

								<!-- Left Nav -->
								<div class="col-md-3">
									<aside class="sidebar">
										<ul class="nav nav-list blog-categories">
											<li>
												<a href="#"> Section 1 </a>
											</li>
											<li>
												<a href="#"> Section 2 </a>
											</li>
											<li>
												<a href="#"> Section 3 </a>
											</li>
											<li>
												<a href="#"> Section 4 </a>
											</li>
											<li>
												<a href="#"> Section 5 </a>
											</li>
											<li>
												<a href="#"> Section 64 </a>
											</li>
										</ul>
									</aside>
								</div>

								<!-- Content Right -->
								<div class="col-md-9">
									<div class="articles-list">

										<article>
											<div class="row">
												<div class="col-md-12">
													<div class="post-content">
														<h2>Working Parents Membership</h2>
														<h4>Corporate Program</h4>
														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae. [...]
														</p>

														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae. [...]
														</p>
													</div>
												</div>

												<!-- article images -->
												<div class="col-md-12 related-items">
													<div data-appears-group-delay="0" data-appears-delay-increase="300">
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body small">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body small">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body small">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Two Lines</a></p>
																</div>
															</div>
														</div>
													</div>

												</div>
												
											</div>										
										</article>

										<hr/>


										<article>
											<div class="row">
												<div class="col-sm-12">
													<div class="post-content">
														<h2>Career Coachcing</h2>
														<h4>Pellentesque pellentesque tempor tellus eget hendrerit tristique in semper vel</h4>
														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.
														</p>
														<p class="text-dark">
																Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. 
														</p>
														<p class="text-dark">
															Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. 
														</p>
														<ul>
															<li>Euismod atras</li>
															<li>Euismod atras</li>
															<li>Euismod atras</li>
															<li>Euismod atras</li> 	
														</ul>
														
													</div>

												</div>

												<div class="col-md-12 margin-top-30 margin-bottom-30"><button type="button" class="btn btn-wide btn-dark">View our team of Executive Coaches</button></div>								
											</div>
										</article>


										<hr/>


										<article>
											<div class="row">

												<div class="col-sm-12">
													<div class="post-content">
														<h2>Career Coachcing</h2>
														<h4>Pellentesque pellentesque tempor tellus eget hendrerit tristique in semper vel</h4>
														<p class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.
														</p>
														<p class="text-dark">
																Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. 
														</p>
														<p class="text-dark">
															Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. 
														</p>
														<ul>
															<li>Euismod atras</li>
															<li>Euismod atras</li>
															<li>Euismod atras</li>
															<li>Euismod atras</li> 	
															<li>Euismod atras</li>
															<li>Euismod atras</li>
															<li>Euismod atras</li>
														</ul>
														
														<p class="text-dark">
															Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. 
														</p>
														<ul>
															<li>Euismod atras</li>
															<li>Euismod atras</li>
															<li>Euismod atras</li>
														</ul>

													</div>
												</div>

												<div class="col-sm-7">
													<div class="post-content padding-top-30">
														<h4 class="text-dark">
															Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos.Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos.
														</h4>
													</div>
												</div>
												<div class="col-sm-5">
													<div class="padding-top-30">
														<img src="assets/images/img1.jpg" class="img-responsive" alt="">
													</div>
												</div>
											</div>
										</article>

										<hr/>


										<article>
											<div class="row">
												<div class="col-sm-12">	
													<h2>Section Title</h2>
													<h4>Sub Title (Optional)</h4>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.
													</p>
												</div>
											</div>
										</article>

										<hr />

										<article>
											<div class="row">
												<div class="col-sm-12">	
													<h2>Special Events</h2>
													<h4>Sub Title (Optional)</h4>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.
													</p>

												</div>

												<div class="col-md-12 margin-top-30 margin-bottom-30"><button type="button" class="btn btn-wide btn-dark">View our upcoming free special events</button></div>

											</div>
										</article>

										<hr />


										<article>
											<div class="row">
												<div class="col-sm-12">	
													<h2>Career Concierge</h2>
													<h4>Sub Title (Optional)</h4>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.
													</p>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar.
													</p>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque 
													</p>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar.
													</p>
												</div>

												<div class="col-md-12 margin-top-30 margin-bottom-30"><button type="button" class="btn btn-wide btn-dark">Enrique about Career Concierge </button></div>

											</div>
										</article>

										<hr />

										<article>
											<div class="row">
												<div class="col-sm-12">	
													<h2>Membership Benefits</h2>
													<h4>Sub Title (Optional)</h4>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.
													</p>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar.
													</p>
												</div>

												<div class="col-md-12 margin-top-30 margin-bottom-30"><button type="button" class="btn btn-wide btn-dark">Read More about the Benefits</button></div>	
											</div>
										</article>

										<hr />

										<article>
											<div class="row">
												<div class="col-sm-12">	
													<h2>The Parents At Work Difference</h2>
													<h4>Sub Title (Optional)</h4>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.
													</p>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non metus. pulvinar. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.  pulvinar.
													</p>
												</div>

												<div class="col-md-12 margin-top-30 margin-bottom-30"><button type="button" class="btn btn-wide btn-dark">Learn More About Us</button></div>	
											</div>
										</article>

										<hr />

									</div>
								</div>
							</div>

						</div>
					</section>
				</div>
				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
