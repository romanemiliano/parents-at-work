
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content">
					<section id="page-title">
						<div class="container">
							<div class="row">
								<div class="col-sm-8">
									<h1 class="mainTitle">Up two line caption <br/>decribing page. </h1>
								</div>
							</div>
						</div>
					</section>
					<section class="container-fluid container-fullw bg-white">
						<div class="container">

							<div class="row">

								<!-- Left Nav -->
								<div class="col-md-3">
									<aside class="sidebar">
										<h4>About us</h4>
										<ul class="nav nav-list blog-categories">
											<li>
												<a href="#"> Our Story </a>
											</li>
											<li>
												<a href="#"> Section 2 </a>
											</li>
											<li>
												<a href="#"> Section 3 </a>
											</li>
											<li>
												<a href="#"> Section 4 </a>
											</li>
											<li>
												<a href="#"> Section 5 </a>
											</li>
											<li>
												<a href="#"> Section 64 </a>
											</li>
										</ul>
									</aside>
								</div>

								<!-- Content Right -->
								<div class="col-md-9">
									<div class="temp-10">

										<article>
											<div class="row">
												<div class="col-md-12">
													<h2 class="text-left">Our Story</h2>
												</div>
												<div class="col-md-6">
													<h4 class="over-title margin-bottom-15">It all began with Mums At Work...</h4>
													<p>
														Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
													</p>
												</div>
												<div class="col-md-6">
													<div class="margin-bottom-30">
														<img src="http://placehold.it/555x370" class="img-responsive" alt="">
													</div>
												</div>
												<div class="col-md-12">
													<p>
														Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
													</p>
												</div>
												<div class="col-md-12 margin-top-30 margin-bottom-30"><button type="button" class="btn btn-wide btn-dark">Learn Why We Core</button></div>
												<div class="col-md-12">
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
													</p>
					                    		</div>	

												<div class="col-md-12 margin-top-30 margin-bottom-30"><button type="button" class="btn btn-wide btn-dark">View our Awards</button></div>

											</div>
										</article>

										<hr/>

										<article>
											<div class="row">
												<div class="col-md-12">
													<h2>Section Title</h2>
													<h4>Sub Title (Optional)</h4>
													<p class="text-dark">
														Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae.Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem. Proin rhoncus consequat nisl, eu ornare mauris tincidunt vitae. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula.
													</p>

													<p class="text-dark">
														Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Pellentesque pellentesque tempor tellus eget hendrerit. Morbi id aliquam ligula. Aliquam id dui sem.
													</p>
													
													<p class="text-dark">
														Euismod atras vulputate iltricies etri elit per conubia nostra, per inceptos himenaeos. Nulla nunc dui, tristique in semper vel, congue sed ligula.
													</p>
												</div>

												<div class="col-md-12 margin-top-30 margin-bottom-30"><button type="button" class="btn btn-wide btn-dark">Read Client Case Studies</button></div>

												<!-- Post articles Images -->
												<div class="col-md-12 padding-top-30">
													<h2>Article Title</h2>
													<div data-appears-group-delay="0" data-appears-delay-increase="300" class="row">
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
													</div>
													<div data-appears-group-delay="0" data-appears-delay-increase="300" class="row">
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
													</div>	
													<div data-appears-group-delay="0" data-appears-delay-increase="300" class="row">
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
													</div>					

												</div>

												<div class="col-md-12 margin-top-30 margin-bottom-30"><button type="button" class="btn btn-wide btn-dark">Read Client Testimonial</button></div>

											</div>		
										</article>

										<hr/>



										<article>
											<div class="row">
												<div class="col-md-12 related-items">
													<h2 class="text-left">Our Partners</h2>
													<p>Nulla nunc dui, tristique in semper vel, congue sed ligula</p>
													<div data-appears-group-delay="0" data-appears-delay-increase="300">
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Two Lines</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Two Lines</a></p>
																</div>
															</div>
														</div>
													</div>
												

													<div data-appears-group-delay="0" data-appears-delay-increase="300">
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Two Lines</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title - Two Lines</a></p>
																</div>
															</div>
														</div>
													</div>

												</div>
											</div>
										</article>									
										<hr />



										<article>
											<div class="row">
												<div class="col-md-12">
													<h2 class="text-left">How We Work</h2>
												</div>
											
												<div class="col-md-12">
													<div class="post-media margin-bottom-30">
														<iframe src="http://player.vimeo.com/video/87701971" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" data-aspectratio="0.562" style="width: 100%; height: 450px;"></iframe>
													</div>
												</div>

												<div class="col-md-12">
													<p>
														Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
													</p>

												</div>

												<div class="col-md-12 margin-top-30 margin-bottom-30"><button type="button" class="btn btn-wide btn-dark">Get Started Today</button></div>
											</div>
										</article>

										<hr/>	

										<article>
											<div class="row">
												<div class="col-md-12 related-items">
													<h2 class="text-left">Media</h2>
													<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
												</div>

												<!-- Post articles Images -->
												<div class="col-md-12">
													<div data-appears-group-delay="0" data-appears-delay-increase="300">
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
													</div>					
													<div data-appears-group-delay="0" data-appears-delay-increase="300">
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x200" class="img-responsive margin-bottom-15" alt="">
																</div>
															</div>
														</div>
													</div>		
												</div>
											</div>
										</article>


										<hr/>


									</div>
								</div>
							</div>

						</div>
					</section>
				</div>
				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
