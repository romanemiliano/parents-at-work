
	<?php include ("includes/head.php") ?> 

	<body>


		<div id="app">

			<?php include ("includes/header.php") ?> 

			<!-- Content Site -->
			<div class="app-content">
				
				<div class="main-content">
					<section id="page-title">
						<div class="container">
							<div class="row">
								<div class="col-sm-8 padding-top-30">
									<h1 class="mainTitle">Up to two linecaption<br/>describing Content Template</h1>
									<p class="margin-top-30"><a href="#" class="btn btn-wide btn-dark">Call to Action</a></p>
								</div>
							</div>
						</div>
					</section>
					<section class="container-fluid container-fullw bg-white">
						<div class="container">
							<div class="row">

								<section class="breadcrumb-page">
						            <ol class="breadcrumb">
						              <li><a href="#">Home</a></li>
						              <li><a href="#">Landing Page</a></li>
						              <li class="active">Detail Page</li>
						            </ol>
							    </section>

								<!-- Content -->
								<div class="col-md-12">
									<div class="temp-3">

										<article>
											<div class="row">
												<div class="col-md-12">
													<h2 class="text-left">Single Page Template Title</h2>
												</div>
												<div class="col-md-6">
													<h4 class="over-title margin-bottom-15">Sub Title (Optional)</h4>
													<p>
														Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
													</p>
												</div>
												<div class="col-md-6">
													<div class="margin-top-30">
														<img src="https://dummyimage.com/435x225/cccccc" class="img-responsive" alt="">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 padding-top-30">
													<h4 class="over-title margin-bottom-15">Sub Title (Optional)</h4>
													<p>
														Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
													</p>
												</div>
												<div class="col-md-6 padding-top-30">
													<h4 class="over-title margin-bottom-15">Sub Title (Optional)</h4>
													<p>
														Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
													</p>
													<p>
														Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
													</p>
												</div>

												<div class="col-md-12 margin-top-30 margin-bottom-30">
													<button type="button" class="btn btn-wide btn-dark pull-left">Call to Action</button>
												</div>
											</div>

										</article>


										<hr/>

										<article>
											<div class="row">
												<div class="col-md-10">
													<h2 class="text-left">Lid est laborum dolo rumes fugats untras</h2>
													<p>Etharums ser quidem rerum facilis dolores</p>
												</div>
												<div class="col-md-2">
													<button type="button" class="btn btn-wide btn-dark margin-top-20">Call to Action</button>
												</div>
											</div>
										</article>

										<hr/>

										<article>
											<div class="row">
												<div class="col-md-12 text-center">
													<h2>"Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes".</h2>
													<p>Lorem ipsum dolor sit amet, consectetur</p>
												</div>
											</div>
										</article>

										<hr/>

										<article>
											<div class="row">
												<div class="col-md-12 related-items">
													<h2 class="text-left">Related Pages</h2>
													<p>You may also interesed in...</p>
													
													
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title <br/> Two Lines</a></p>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="text-center no-visible fadeIn animated" data-appears-class="fadeIn" data-appears-delay="0">
																<div class="panel-body">
																	<img src="http://placehold.it/330x250" class="img-responsive margin-bottom-15" alt="">
																	<p><a href="#">Single Page Title <br/> Two Lines</a></p>
																</div>
															</div>
														</div>
													

												</div>
											</div>
										</article>									

										<hr />

									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				
				<?php include ("includes/footer.php") ?> 

			</div>
			<!-- /. Content Site -->
		</div>


		<?php include ("includes/plugins.php") ?> 

	</body>
</html>
